'use strict';

var express = require('express');
var axios = require('axios');
const fs = require('fs');
var bodyParser = require('body-parser');
const uuid = require('uuid');

var hostname = 'localhost';
var port = 8050;

var app = express();

var router = express.Router();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// writing on users.json
app.post('/user/new', function (req, res) {
    fs.readFile('./users.json', 'utf-8', function (err, data) {
        if (err) throw err

        var arrayOfUsers = JSON.parse(data);
        req.body.id = uuid.v4();
        arrayOfUsers.users.push(req.body);
        console.log(arrayOfUsers);
        
        fs.writeFile('./users.json', JSON.stringify(arrayOfUsers, null, 2), 'utf-8', function(err){
		if (err) throw err
        console.log('Done!')
        res.redirect('http://localhost:8080/userlist');
        })
        
    });
    
});

// displaying of users.json
app.get('/user', function (req, res){
    fs.readFile("./users.json",'utf-8', function (err, jsonData) {
        if (err) {
            console.log("Error reading file:", err);
            return;
        }
        try {
            var dataObjet = JSON.parse(jsonData);
            res.send(dataObjet);
        }
        catch (err) {
            console.log("Error parsing json:", err);
        }
    });
});
    
app.delete('/user/:id', function (req, res) {

    let id = req.params.id;
    fs.readFile('./users.json', function (err, data) {
        let user = JSON.parse(data);
        console.log(user);
        for(var i = 0; i < user.users.length; i++) {
            if (id == user.users[i].id) {
                user.users.splice(i, 1);
                break;
            }
        }
        let users_string = JSON.stringify(user, null, 2);
        fs.writeFile('./users.json', users_string, (err) => {
            if (err) throw err;
        })

    });
    res.status(200).send();
});

app.listen(port, hostname, function () {
    console.log("Mon serveur API fonctionne sur http://" + hostname + ":" + port);
});

