var express = require('express');
var ejs = require('ejs');
var axios = require('axios');
var bodyParser = require('body-parser');
//var urlencodedParser = bodyParser.urlencoded({ extended: true });

var app = express();

app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'ejs');

app.get('/', function (req, res) {
    res.render('./pages/main.ejs', { page: "form", tabusers:""});
});

app.post('/form', async function (req, res) {
    console.log(req.body);
    var sendReq = await axios.post('http://localhost:8050/user/new', req.body);
    res.redirect('http://localhost:8080/userlist');
});

app.get('/userlist', async function (req, res) {
    var resultReq = await axios.get('http://localhost:8050/user');
    let resultString = JSON.stringify(resultReq.data.users);
    let usersObjet = JSON.parse(resultString);
    res.render('./pages/main.ejs', { page: "userlist", tabusers: usersObjet});
});

app.get('/user/:id', function (req, res) {
    let id = req.params.id;
    console.log('mon ' + id)
    axios.delete("http://localhost:8050/user/" + id)
        .then(function (resp) {
            console.log(resp)
            res.redirect("http://localhost:8080/userlist")

            console.log('Yes!!');
            console.log('we deleted data!!');

        })
        .catch(function (err) {
            //console.log(err);
            console.log('louper');

            res.status(404).send();

        });
});

app.use(express.static(__dirname + '/views'));
app.listen(8080);



